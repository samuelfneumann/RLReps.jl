# RLReps.jl: Representations for RL

Fixed representations for Reinforcement Learning in Julia.

So far implemented:
	Grid-based Tile coder

Future:
	Basis functions (e.g. RBF)
	Hash-based tile coder

