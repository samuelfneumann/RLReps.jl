module RLReps
using Random

include("AbstractTiling.jl")
include("TileCoder.jl")
include("GridTiling.jl")
include("HashTiling.jl")

export AbstractTiling, TileCoder
export GridTileCoder, GridTiling
export features, index, onehot, nonzero, sum_to_one

end
